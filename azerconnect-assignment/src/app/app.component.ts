import {Component, OnInit} from '@angular/core';
import {ProductService} from "./core/services/product.service";
import {Product} from "./core/interfaces/product";
import {Control} from "./core/enum/control";
import {ToastrService} from "ngx-toastr";
import {LocalStorageService} from "./core/services/local-storage.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  loading: boolean = false;
  products: Product[] = [];
  control = Control;

  constructor(
    private productService: ProductService,
    private toastrService: ToastrService,
    private localStorageService: LocalStorageService
  ) {
  }

  ngOnInit(): void {
    this.getAllProducts();
  }

  getAllProducts(): void {
    this.loading = true;
    this.productService.getAllProducts().subscribe((body) => {
      this.products = body.map((v: Product) => ({
        ...v,
        count: 0
      }));
      this.localStorageService.compareData(this.products);

    }).add(() => this.loading = false);
  }

  addProduct(product: Product): void {
    product.count! = 1;
    this.localStorageService.saveBasket(this.products);
  }

  totalPrice(): number {
    let sum = 0;
    this.products.filter((p: Product) => p!.count! >= 1).forEach((p: Product) => {
      sum += p!.count! * p.price;
    });
    return Number(sum.toFixed(2));
  }

  getBasket(): Product[] {
    return this.products.filter((p: Product) => p!.count! >= 1);
  }

  deleteProductById(id: number): void {
    const product = this.products.find((v: Product) => v.id === id);
    product!.count! = 0;
    this.localStorageService.saveBasket(this.products);
  }

  productControl(id: number, control: Control): void {
    const product = this.products.find((v: Product) => v.id === id);
    if(control === Control.INCREASE){
      product!.count!++;
    } else {
      if(product!.count! === 1){
        return;
      }
      product!.count!--;
    }
    this.localStorageService.saveBasket(this.products);
  }

  checkOut(): void {
    if (this.getBasket().length === 0){
      this.toastrService.info('Basket is empty');
      return;
    }
    this.toastrService.success('Success');
    this.products.forEach((p: Product) => {
      p.count = 0;
    });
    this.localStorageService.saveBasket(this.products);
  }


}
