export class HttpConf {

  private static REST_API = 'http://localhost:3000';


  public static URL = {
    products: `${HttpConf.REST_API}/products`
  }

}
