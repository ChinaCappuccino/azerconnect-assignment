export interface Product {
  id: number;
  title: string;
  description: string;
  price: number;
  imagePath: string;
  count?: number;
}
