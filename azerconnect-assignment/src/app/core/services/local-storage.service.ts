import { Injectable } from '@angular/core';
import {Product} from "../interfaces/product";

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  saveBasket(body: Product[]): void {
    localStorage.setItem('basket', JSON.stringify(body));
  }

  getBasket(): Product[] {
    return JSON.parse(localStorage.getItem('basket')! ?? '[]');
  }

  compareData(requestData: Product[]): void {
    this.getBasket().forEach((v: Product) => {
      requestData.forEach((p: Product) => {
        if (v.id === p.id){
          p.count = v.count;
        }
      });
    });
  }

}
