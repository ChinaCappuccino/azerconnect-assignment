import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Product} from "../interfaces/product";
import {HttpConf} from "../http/http.conf";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private http: HttpClient
  ) { }


  getAllProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(`${HttpConf.URL.products}`);
  }

}
